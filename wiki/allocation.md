---
title: Allocation
author: J.W. Morsink
---

# Allocation

Allocation is the assignment of funds, based on profits, to be paid out to [charities](./charity). 
It is determined based on the exit amount specified in the [CONV_EXIT](./events/CONV_EXIT) event and the fraction of ownership that charities have of the [investment option](./option).
