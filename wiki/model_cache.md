---
title: Give for Good - Model Cache
author: J.W. Morsink
archimate: 
    caption: Model Cache
    layer: Application
    type: Service
    serves:
    - to: calculator
---

# Model Cache

The model cache is responsible for caching instances of [models](./calculator#models) for certain points in the event sequence, to speed up calculations.

